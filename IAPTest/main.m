//
//  main.m
//  IAPTest
//
//  Created by Michael Rebello on 1/12/16.
//  Copyright © 2016 Michael Rebello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
