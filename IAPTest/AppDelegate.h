//
//  AppDelegate.h
//  IAPTest
//
//  Created by Michael Rebello on 1/12/16.
//  Copyright © 2016 Michael Rebello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

