//
//  ViewController.m
//  IAPTest
//
//  Created by Michael Rebello on 1/12/16.
//  Copyright © 2016 Michael Rebello. All rights reserved.
//

#import "ViewController.h"

#import <RMStore/RMStore.h>

@interface ViewController ()

@end

@implementation ViewController

#define kMyInAppPurchaseIdentifier @"com.michaelrebello.testpurchase1"

//RMStore is the library used in this project for handling in-app purchases
//Their library supports CocoaPods, and is available here:
//https://github.com/robotmedia/RMStore

-(void)loadProducts {
    
    //A list of your product identifiers in iTunes Connect
    NSSet *products = [NSSet setWithArray:@[kMyInAppPurchaseIdentifier]];
    
    //Load products (and prices, etc.) from iTunes Connect
    [[RMStore defaultStore] requestProducts:products success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
        
        readyToPurchase = TRUE;
        statusLabel.text = @"Products Loaded";
        if (products.count > 0) priceLabel.text = [self priceForProduct:products[0]];
        
        NSLog(@"Loaded products: %@", products);

    } failure:^(NSError *error) {
        
        NSLog(@"Failed to load products: %@", error);
    }];
}

-(IBAction)purchaseProduct {
    
    if (!readyToPurchase) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Loading Products" message:@"We're still loading products. Try again in a moment." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //Start purchasing a product
    [[RMStore defaultStore] addPayment:kMyInAppPurchaseIdentifier success:^(SKPaymentTransaction *transaction) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Product Purchased!" message:@"Your purchase has been processed successfully." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        
        NSLog(@"Product purchased: %@", transaction.payment.productIdentifier);
        
    } failure:^(SKPaymentTransaction *transaction, NSError *error) {

        if (transaction.error.code != SKErrorPaymentCancelled) {

            NSLog(@"Product purchase failed: %@", error);
        }
    }];
}

//Returns the price of an SKProduct
//Reference: http://stackoverflow.com/questions/2894321/how-to-access-the-price-of-a-product-in-skpayment
-(NSString *)priceForProduct:(SKProduct *)product {
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:product.priceLocale];
    
    return [formatter stringFromNumber:product.price];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadProducts];
}

@end