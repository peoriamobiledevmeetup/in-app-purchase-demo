//
//  ViewController.h
//  IAPTest
//
//  Created by Michael Rebello on 1/12/16.
//  Copyright © 2016 Michael Rebello. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKProduct;

@interface ViewController : UIViewController {
    
    BOOL readyToPurchase;
    
    IBOutlet UILabel *priceLabel, *statusLabel;
}

-(void)loadProducts;
-(IBAction)purchaseProduct;
-(NSString *)priceForProduct:(SKProduct *)product;

@end

